const { spawn } = require("child_process");
const { readJson, remove } = require("fs-extra");

// -- Main Tasks -------------------------------------------------------------------------------- //

async function lint() {
  await exec(`npx tslint --exclude 'node_modules/**' '**/*.[jt]s'`);
}

async function test() {
  await exec("npx jest");
}

async function test_appmesh() {
  await exec("npx jest test/appmesh");
}

async function test_ecr() {
  await exec("npx jest test/ecr");
}

async function test_ecs() {
  await exec("npx jest test/ecs");
}

// -- Package Tasks ----------------------------------------------------------------------------- //

async function package_clean() {
  await remove("node_modules");
  await remove("package-lock.json");
}

async function package_update() {
  const package = await readJson("package.json");
  const dependencies = Object.keys(package.dependencies || {}).join(" ");
  const devDependencies = Object.keys(package.devDependencies || {}).join(" ");
  await exec(`npm install --save-prod ${dependencies}`);
  await exec(`npm install --save-dev ${devDependencies}`);
}

async function package_update_latest() {
  const package = await readJson("package.json");
  const dependencies = Object.keys(package.dependencies || {}).map(x => x + "@latest").join(" ");
  const devDependencies = Object.keys(package.devDependencies || {}).map(x => x + "@latest").join(" ");
  await exec(`npm install --save-prod ${dependencies}`);
  await exec(`npm install --save-dev ${devDependencies}`);
}

// -- Helper Functions -------------------------------------------------------------------------- //

function exec(command) {
  return new Promise((resolve, reject) => {
    const [cmd, ...args] = command.split(" ");
    spawn(cmd, args, { shell: true, stdio: "inherit" })
      .on("error", (err) => reject(err))
      .on("close", (code) => resolve(code));
  });
}

// -- Exports ----------------------------------------------------------------------------------- //

module.exports = {
  lint,
  package_clean,
  package_update,
  package_update_latest,
  test,
  test_appmesh,
  test_ecr,
  test_ecs,
};
