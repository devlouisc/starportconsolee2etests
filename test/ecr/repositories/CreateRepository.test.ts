import { accountId, password, username } from "@src/AwsConfig";
import { AwsConsole } from "@src/AwsConsole";
import { AwsRegion } from "@src/AwsRegion";

jest.setTimeout(5 * 60_000);

describe("Create repository page", () => {
  const awsConsole = new AwsConsole();

  beforeAll(async () => {
    await awsConsole.launch();
    await awsConsole.signIn(accountId, username, password);
    await awsConsole.changeRegion(AwsRegion.US_WEST_2);
    await awsConsole.goToEcrService();
  });

  afterAll(async () => {
    await awsConsole.signOut();
    await awsConsole.close();
  });

  test("create a new repository", async () => {
    // await awsConsole.ecrRepositoriesPage.clickCreateRepositoryButton();
    // await awsConsole.ecrCreateRepositoryPage.fillRepositoryNameField("");
  });
});
