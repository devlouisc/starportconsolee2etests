# Design

- I want to be able to test with different accounts.
- I want to be able to start a test with a clean slate.

```js
import { awsConsole } from "@src";
import { ecrRepositoriesPage } from "@src/pages";

const awsConsole = new AwsConsole();
await awsConsole.launch();
await awsConsoel.close():

await awsConsole.signIn(AwsRegion.US_WEST_2, "accountId", "username", "password");
  .then((x) => x.goToEcrRepositoriesPage())
  .then((x) => x.signOut());

await awsConsole.clearCache();
await awsConsole.clearCookies();

const formValues = {
  name: "Adam",
  age: 25,
}

const results = await awsConsole.ecrRepositoriesPage
  .fillOutCreateRepositoryForm(formValues)
  .then((x) => x.clickSubmit())
  .then((x) => x.getRepositoryName());
```
