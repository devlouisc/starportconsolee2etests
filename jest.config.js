module.exports = {
  moduleNameMapper: {
    "@src/(.*)": "<rootDir>/src/$1",
    "@test/(.*)": "<rootDir>/test/$1",
  },
  roots: ["<rootDir>/test"],
  transform: { "\.ts$": "ts-jest" },
};
