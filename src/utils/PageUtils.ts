import { ElementHandle, Page } from "puppeteer";

/**
 * Gets the selected HTML element.
 */
export async function getElement(page: Page, selector: string): Promise<ElementHandle> {
  const element = await page.$(selector);
  if (element === null) { throw new Error(`No element matched with selector: ${selector}`); }
  return element;
}

/**
 * Clears the selected text field by pressing backspace as many times as there
 * are characters in the text field.
 */
export async function clearTextField(page: Page, selector: string): Promise<void> {
  await page.focus(selector);
  const value: string = await page.$eval(selector, (el: any) => el.value);
  [...value].forEach(async () => await page.keyboard.press("Backspace"));
}
