import { getElement } from "@src/utils/PageUtils";
import { Page } from "puppeteer";

export class EcrRepositoriesPage {
  public constructor(private readonly page: Page) {}

  public async clickCreateRepositoryButton(): Promise<void> {
    await getElement(this.page, "#createRepositoryButton").then((x) => x.click());
  }
}
