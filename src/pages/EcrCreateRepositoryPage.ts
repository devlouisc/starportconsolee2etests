import { IEcrCreateRepositoryFormValues } from "@src/models";
import { getElement } from "@src/utils/PageUtils";
import { ElementHandle, Page } from "puppeteer";

export class EcrCreateRepositoryPage {
  public constructor(private readonly page: Page) {}

  public async fillForm(values: IEcrCreateRepositoryFormValues): Promise<void> {
    // await this.getRepositoryNameTextField().then((x) => x.type(values.repositoryName));
    // await this.getImageTagMutabilityRadioField().then((x) => x.click());
    // await this.getCreateRepositoryButton().then((x) => x.click());
  }

  // -- Page Elements --------------------------------------------------------------------------- //

  private getRepositoryNameTextField(): Promise<ElementHandle> {
    return getElement(this.page, "#repositoryNameTextField");
  }

  private getImageTagMutabilityRadioField(): Promise<ElementHandle> {
    return getElement(this.page, "#imageTagMutabilityRadioField");
  }

  private getCancelButton(): Promise<ElementHandle> {
    return getElement(this.page, "#cancelButton");
  }

  private getCreateRepositoryButton(): Promise<ElementHandle> {
    return getElement(this.page, "#createRepositoryButton");
  }
}
