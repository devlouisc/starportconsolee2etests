import { AwsRegion } from "@src/AwsRegion";
import { EcrCreateRepositoryPage, EcrRepositoriesPage } from "@src/pages";
import { getElement } from "@src/utils/PageUtils";
import { Browser, launch, Page } from "puppeteer";
import { URL } from "url";

export class AwsConsole {
  private readonly AWS_CONSOLE_BASE_URL = "https://console.aws.amazon.com";
  private browser: Browser;
  private page: Page;

  // -- Browser Management ---------------------------------------------------------------------- //

  public async launch(): Promise<void> {
    if (!this.browser) {
      this.browser = await launch({ headless: false });
      this.page = (await this.browser.pages())[0];
    }
  }

  public async close(): Promise<void> {
    if (this.browser) {
      await this.browser.close();
      delete this.browser;
      delete this.page;
    }
  }

  // -- Authentication -------------------------------------------------------------------------- //

  public async signIn(accountId: string, username: string, password: string): Promise<void> {
    const signInUrl = `https://${accountId}.signin.aws.amazon.com/console`;
    await this.page.goto(signInUrl);

    await getElement(this.page, "#username").then((x) => x.type(username));
    await getElement(this.page, "#password").then((x) => x.type(password));

    // Clicking the sign in button performs more than one implicit navigation so
    // we are waiting on a selector rather than on a navigation.
    await Promise.all([
      this.page.waitForSelector("#aws-services"),
      getElement(this.page, "#signin_button").then((x) => x.click()),
    ]);
  }

  public async signOut(): Promise<void> {
    const signOutUrl = "https://console.aws.amazon.com/console/logout!doLogout";
    await this.page.goto(signOutUrl);
  }

  // -- Navigation ------------------------------------------------------------------------------ //

  public async changeRegion(region: AwsRegion): Promise<void> {
    const url = new URL(this.page.url());
    url.hostname = url.hostname.split(".").map((x, i) => i === 0 ? region : x).join(".");
    url.searchParams.set("region", region);

    await this.page.goto(url.toString());
  }

  public async goToAppMeshService(): Promise<void> {
    await this.page.goto(`${this.AWS_CONSOLE_BASE_URL}/appmesh`);
  }

  public async goToEcrService(): Promise<void> {
    await this.page.goto(`${this.AWS_CONSOLE_BASE_URL}/ecr`);
  }

  public async goToEcsService(): Promise<void> {
    await this.page.goto(`${this.AWS_CONSOLE_BASE_URL}/ecs`);
  }

  // -- AWS Service Pages ----------------------------------------------------------------------- //

  public get ecrCreateRepositoryPage(): EcrCreateRepositoryPage { return new EcrCreateRepositoryPage(this.page); }
  public get ecrRepositoriesPage(): EcrRepositoriesPage { return new EcrRepositoriesPage(this.page); }
}
