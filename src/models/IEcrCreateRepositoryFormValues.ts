export interface IEcrCreateRepositoryFormValues {
  repositoryName?: string;
  imageTagMutability?: "MUTABLE" | "IMMUTABLE";
}
