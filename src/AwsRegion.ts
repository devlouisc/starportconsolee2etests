export enum AwsRegion {
  AP_EAST_1 = "ap-east-1",            // Asia Pacific (Hong Kong)
  AP_NORTHEAST_1 = "ap-northeast-1",  // Asia Pacific (Tokyo)
  AP_NORTHEAST_2 = "ap-northeast-2",  // Asia Pacific (Seoul)
  AP_NORTHEAST_3 = "ap-northeast-3",  // Asia Pacific (Osaka-Local)
  AP_SOUTH_1 = "ap-south-1",          // Asia Pacific (Mumbai)
  AP_SOUTHEAST_1 = "ap-southeast-1",  // Asia Pacific (Singapore)
  AP_SOUTHEAST_2 = "ap-southeast-2",  // Asia Pacific (Sydney)

  CA_CENTRAL_1 = "ca-central-1",      // Canada (Central)

  CN_NORTH_1 = "cn-north-1",          // China (Beijing)
  CN_NORTHWEST_1 = "cn-northwest-1",  // China (Ningxia)

  EU_CENTRAL_1 = "eu-central-1",      // EU (Frankfurt)
  EU_NORTH_1 = "eu-north-1",          // EU (Stockholm)
  EU_WEST_1 = "eu-west-1",            // EU (Ireland)
  EU_WEST_2 = "eu-west-2",            // EU (London)
  EU_WEST_3 = "eu-west-3",            // EU (Paris)

  SA_EAST_1 = "sa-east-1",            // South America (São Paulo)

  US_GOV_EAST_1 = "us-gov-east-1",    // AWS GovCloud (US-East)
  US_GOV_WEST_1 = "us-gov-west-1",    // AWS GovCloud (US)
  US_EAST_1 = "us-east-1",            // US East (N. Virginia)
  US_EAST_2 = "us-east-2",            // US East (Ohio)
  US_WEST_1 = "us-west-1",            // US West (N. California)
  US_WEST_2 = "us-west-2",            // US West (Oregon)
}
