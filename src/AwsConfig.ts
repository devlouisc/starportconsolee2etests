// WARNING: DO NOT CHECK YOUR CREDENTIALS INTO SOURCE CONTROL!!!
import { AwsRegion } from "@src/AwsRegion";

export const accountId = process.env.AWS_ACCOUNT_ID || "";
export const region = process.env.AWS_REGION || AwsRegion.US_WEST_2;

export const username = process.env.AWS_USERNAME || "";
export const password = process.env.AWS_PASSWORD || "";

export const accessKey = process.env.AWS_ACCESS_KEY || "";
export const secretKey = process.env.AWS_SECRET_KEY || "";
